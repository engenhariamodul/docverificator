def relistar(doc, saida):
	"""
	:param doc: Nome do documento que será recebido no formato inicial
	:param saida: Nome do documento que será criado com a saida.
	:return: Retorna a variavel 'saida'.
	"""
	lista = list()
	listaFinal = list()
	adicionar = True   		#Não está sendo usado inicialmente, mas não deletar!
	comparador = ''
	for i in doc:
		lista.append(i.rstrip())

	for item in lista:
		adicionar = True
		for x in range(0, 4):
			comparador += item[x]

		if len(listaFinal) == 0:
			listaFinal.append(item)
		else:
			for itens in listaFinal:
				if comparador in itens:
					adicionar = False

			if adicionar:
				listaFinal.append(item)

		comparador = ''
	docSaida = open(saida, 'w')
	for item in listaFinal:
		docSaida.writelines(item+'\n')



documento = open('lista.txt', 'r')
relistar(documento, 'docSaida')